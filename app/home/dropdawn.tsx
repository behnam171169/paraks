"use client";
import React, { useState } from "react";
import { setpokmonname } from "./../redux/slicess/pokmonname";
import { AppDispatch, RootState } from "@/app/redux/store/store";
import { useDispatch, useSelector } from "react-redux";
// ********* types *********************** types ************************* types ****************************
type Iprops = {
  classname: string;
  data: { name: string; url: string }[];
  onclicktab: Function;
  title?: string;
};

type tabdata = {
  name: string;
  url: string;
};
// ********* main *************************main **************************main **************
function Dropdawn({ data, classname, title, onclicktab }: Iprops) {
  const { pokmonname } = useSelector((state: RootState) => state.pokmon);
  const dispatch = useDispatch<AppDispatch>();
  const [open, setOpen] = useState(false);

  const onclickTab = (data: tabdata) => {
    dispatch(setpokmonname(data.name));
    onclicktab(data.url);
    setOpen(false);
  };
  
  return (
    <div className={`h-auto flex flex-col ${classname}`}>
      {title ? (
        <span className="text-[16px] leading-[22px] font-normal text-[16px] text-[#545F71] -tracking-2 py-2">
          {title}
        </span>
      ) : null}

      <div className="w-full relative">
        <button
          onClick={() => setOpen(!open)}
          className={`h-[48px] rounded-[6px] border-[1px] border-[#545F71] border-solid bg-[#FFFFFF] text-left text-[#545F71] border-solid w-full py-[13px] px-[12px]`}
        >
          {pokmonname}
        </button>
        {open ? (
          <div className="w-[274.82px] bg-[#FFFFFF] h-[48px] flex flex-col absolute h-[auto] max-h-[240px] overflow-y-auto top-[50px] z-10 left-0 right-0 w-full">
            {data.map((data: { name: string; url: string }, index: number) => (
              <button
                key={index}
                onClick={() => onclickTab(data)}
                className="w-full h-[48px] text-[16px] leading-[22px] font-normal text-[16px] text-[#545F71] -tracking-2 text-left py-[13px] px-[12px]"
              >
                {data.name}
              </button>
            ))}
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default Dropdawn;
