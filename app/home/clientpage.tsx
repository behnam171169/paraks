"use client";
import React, { useMemo } from "react";
import { useParams } from "next/navigation";
import Homeright from "../components/homeright";
import { BsArrowLeftCircle, BsChevronDown } from "react-icons/bs";
import { useRouter } from "next/navigation";
import {
  useGetPokemonByNameQuery,
  useGetPokemondataByNameQuery,
} from "./../redux/api/apiSlice";
import Dropdawn from "./dropdawn";

function Page() {
  const params = useParams();

  const router = useRouter();

  const { data } = useGetPokemonByNameQuery();

  const { data: alldata = [] } = useGetPokemondataByNameQuery(
    Number(params.id)
  );
  console.log(alldata, "lldata");

  const abilitylist = useMemo(() => {
    let abiliydata = [];
    for (let i = 0; i < alldata?.abilities?.length; i++) {
      abiliydata.push(alldata.abilities[i].ability.name);
    }
    return abiliydata;
  }, [params]);

  const statslist = useMemo(() => {
    let statsdata = [];
    for (let i = 0; i < alldata?.stats?.length; i++) {
      statsdata.push(alldata.stats[i].stat.name);
    }
    return statsdata;
  }, [params]);

  return (
    <div className="flex min-h-screen flex-col pt-[85px] bg-[#353841] ">
      <div className="w-[977.51px] h-[560px] flex flex-row justify-between mx-auto">
        <div className="w-[350px] h-[560px] bg-[#2D2F32] rounded-[10px] relative">
          <BsChevronDown class="absolute top-[-18px] left-[15px] w-[19.2px] h-[19.2px] text-[#545F71]" />
          <Dropdawn
            onclicktab={(data: string) =>
              router.push(data.split("/")[data.split("/").length - 2])
            }
            classname={`w-[274.82px] mx-auto`}
            data={data?.results}
            title="Select a Pokemon"
          />
          <img
            className="w-[227.63px] h-[227.63px] mx-auto mt-[8px] mb-[15px]"
            src={
              "https://roozaneh.net/wp-content/uploads/2018/07/aks-gol-31.jpg.webp"
            }
            alt={""}
          />
          <div className="w-[299px] flex flex-col mx-auto mb-[25px]">
            <span className="text-[20px] leading-[22px] font-normal text-[16px] text-[#545F71] -tracking-2 py-2">
              Short Description
            </span>
            <span className="text-[16px] leading-[22px] font-normal text-[16px] text-[#545F71] -tracking-2 mt-[10px]">
              a strange seed was planted on it’s back at birth. the plant
              sprouts and grows with this Pokemon.
            </span>
          </div>
          <div className="w-[299px] flex flex-row justify-between mx-auto">
            <button className="w-[126px] h-[48px] rounded-[6px] flex flex-row  py-[12px] px-[16px]  bg-[#545F71]">
              <BsArrowLeftCircle className="w-[24px] h-[24px] mr-[4px] text-[#FFFFFF]" />
              <span className="text-[16px] leading-[22px] font-semibold text-[16px] text-[#FFFFFF] -tracking-2">
                Prev
              </span>
            </button>
            <button className="w-[126px] h-[48px] rounded-[6px] flex flex-row py-[12px] px-[16px] bg-[#545F71] justify-end	">
              <span className="text-[16px] leading-[22px] font-semibold text-[16px] text-[#FFFFFF] -tracking-2 mr-[6px]">
                Next
              </span>
              <BsArrowLeftCircle className="w-[24px] h-[24px] rotate-180 text-[#FFFFFF]" />
            </button>
          </div>
        </div>

        <Homeright
          PokemonName={alldata.name}
          Abilities={abilitylist}
          Stats={statslist}
        />
      </div>
    </div>
  );
}

export default Page;
