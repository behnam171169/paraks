import React from "react";

type Iprops = {
  PokemonName: string;
  Abilities: string[];
  Stats: string[];
};
// ---------------main ----------------------- main ------------------- main -----------
function Homeright({ PokemonName, Abilities, Stats }: Iprops) {
  return (
    <div className="w-[615px] h-[560px]  bg-[#2D2F32] rounded-[10px] flex flex-col pl-[20px] pt-[30px]">
      <span className="text-[20px] leading-[22px] font-normal text-[16px] text-[#FFFFFF] -tracking-2 mb-[30px]">
        Pokemon Name:{PokemonName}
      </span>

      <span className="text-[20px] leading-[22px] font-normal text-[16px] text-[#FFFFFF] -tracking-2 ">
        Abilities
      </span>
      <div className="flex flex-row">
        {Abilities.map((data: string, index: number) => (
          <span
            key={index}
            className="flex text-[16px] leading-[22px] font-normal text-[16px] text-[#FFFFFF] -tracking-2 mt-[10px]"
          >
            {data},
          </span>
        ))}
      </div>

      <span className="text-[20px] leading-[22px] font-normal text-[16px] text-[#FFFFFF] -tracking-2 mt-[30px]">
        Stats
      </span>
      <div className="flex flex-row">
        {Stats.map((data: string, index: number) => (
          <span
            key={index}
            className="flex text-[16px] leading-[22px] font-normal text-[16px] text-[#FFFFFF] -tracking-2 mt-[10px]"
          >
            {data},
          </span>
        ))}
      </div>
    </div>
  );
}

export default Homeright;
