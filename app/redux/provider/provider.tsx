"use client";
type childrencomponentprops = {
  children: React.ReactNode;
};

import React from "react";
import { store } from "../store/store";

import { Provider } from "react-redux";
function ReduxProvider({ children }: childrencomponentprops) {
  return <Provider store={store}>{children}</Provider>;
}

export default ReduxProvider;
