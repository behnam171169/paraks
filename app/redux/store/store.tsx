import { configureStore } from "@reduxjs/toolkit";
import { pokemonApi } from "./../api/apiSlice";
import  pokemonslice  from "./../slicess/pokmonname";
export function makeStore() {
  return configureStore({
   reducer: {
    pokmon:pokemonslice,
    [pokemonApi.reducerPath]: pokemonApi.reducer,
  },
    middleware: (getDefaultMiddleware: any) =>
      getDefaultMiddleware().concat(pokemonApi.middleware),
  });
}

export const store = makeStore();

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
