import { createSlice} from "@reduxjs/toolkit";

const initialState = {
  pokmonname:"bulbasaur",
} as any;

const changepokmon = createSlice({
  name: "filter",
  initialState,
  reducers: {
    setpokmonname: (state, action) => {
     state.pokmonname = action.payload;
   },

  },
});

export const {setpokmonname} = changepokmon.actions;

export default changepokmon.reducer;