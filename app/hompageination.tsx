"use client";
import React from "react";
import { BsArrowLeftCircle } from "react-icons/bs";
import { useGetPokemonByNameQuery } from "./redux/api/apiSlice";
function Hompageination() {
  const { data, error, isLoading } = useGetPokemonByNameQuery("bulbasaur");
  console.log(data, "tttt");

  return (
    <div className="w-[299px] flex flex-row justify-between mx-auto">
      <button className="w-[126px] h-[48px] rounded-[6px] flex flex-row  py-[12px] px-[16px]  bg-[#545F71]">
        <BsArrowLeftCircle className="w-[24px] h-[24px] mr-[4px] text-[#FFFFFF]" />
        <span className="text-[16px] leading-[22px] font-semibold text-[16px] text-[#FFFFFF] -tracking-2">
          Prev
        </span>
      </button>
      <button className="w-[126px] h-[48px] rounded-[6px] flex flex-row py-[12px] px-[16px] bg-[#545F71] justify-end	">
        <span className="text-[16px] leading-[22px] font-semibold text-[16px] text-[#FFFFFF] -tracking-2 mr-[6px]">
          Next
        </span>
        <BsArrowLeftCircle className="w-[24px] h-[24px] rotate-180 text-[#FFFFFF]" />
      </button>
    </div>
  );
}

export default Hompageination;
